package com.korn.lab9;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {
    static Map map = new Map(15, 15);
    static Robot robot = new Robot(map, 'A', 10, 10);
    static Unit tree1 = new Tree(map, 5, 5);
    static Unit tree2 = new Tree(map, 5, 6);
    static Unit tree3 = new Tree(map, 6, 5);
    static Unit tree4 = new Tree(map, 7, 5);
    static Unit tree5 = new Tree(map, 5, 7);
    static Unit tree6 = new Tree(map, 6, 7);
    static Unit tree7 = new Tree(map, 11, 9);
    static Unit tree8 = new Tree(map, 11, 10);
    static Unit tree9 = new Tree(map, 11, 12);
    static Unit tree10 = new Tree(map, 12, 10);
    static Scanner sc = new Scanner(System.in);

    public static String input() {
        return sc.next();
    }

    public static void process(String command) {
        switch (command) {
            case "a":
                robot.up();
                break;
            case "d":
                robot.down();
                break;
            case "w":
                robot.left();
                break;
            case "s":
                robot.right();
                break;
            case "q":
                System.out.println("Bye bye!!!");
                System.exit(0);
                break;
            default:
                break;
        }
    }

    public static void main(String[] args) {
        map.add(tree1);
        map.add(tree2);
        map.add(tree3);
        map.add(tree4);
        map.add(tree5);
        map.add(tree6);
        map.add(tree7);
        map.add(tree8);
        map.add(tree9);
        map.add(tree10);
        map.add(robot);
        map.print();

        while (true) {
            map.print();
            String command = input();
            process(command);
        }
    }
}
